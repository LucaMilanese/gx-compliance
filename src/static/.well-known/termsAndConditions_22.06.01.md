The PARTICIPANT signing the Self-Description agrees as follows:
- to update its descriptions about any changes, be it technical, organisational, or legal - especially but not limited to contractual in regards of the indicated attributes present in the descriptions.
- wrongful statements will reflect a breach of contract and may cumulate to unfair competitive behaviour.
- in cases of systematic and deliberate misrepresentations, Gaia-X Association is, without prejudice to claims and rights under the applicable law, entitled to take actions as defined in this document Architecture document - Operation model chapter - Self-Description Remediation section.

Alongside, the PARTICIPANT signing the Self-Description is aware and accepts that:
- the SERVICE OFFERING will be delisted where Gaia-X Association becomes aware of any inaccurate statements in regards of the SERVICE OFFERING which result in a non-compliance with the Trust Framework and Policy Rules document.
