# Introduction

Welcome to Gaia-X Trust Framework service. This service provides the verification of self-descriptions folowing the rules stated in the current version of the [Trust Framework](https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/).

The software is developed by the Gaia-X Lab and is currently in incubation stage. Features that are still under heavy development are marked as _experimental_.


Want to try it out? Check out our [Compliance API](https://compliance.gaia-x.eu/docs/).  
Want to know more about the code? Take a look at our [repo](https://gitlab.com/gaia-x/lab/compliance).  
Want to see what's on the roadmap? See what's in our [backlog](https://gaia-x.atlassian.net/jira/software/c/projects/LAB/boards/10/backlog).  
Want to know more about Gaia-X? Visit our [main site](https://gaia-x.eu/).  
